<?php
	require 'db.php';
    require 'header.php';

	$id = $_GET['id'] ? (int)$_GET['id'] : null;
	$user = $_SESSION['logged_user'] ?? null;
?>

<body>
    <div class="site-wrapper">
        <div class="cover-container">

            <?php
            	require 'navbar.php';
            ?>
			<div class="container">
				<div class="row" style="margin-top: 150px;">
                    <?php if (isset($user)): ?>
                        <h2>Журнал запросов</h2>
                        <table class="table">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Created At</th>
                                <th>IP</th>
                                <th>Description</th>
                              </tr>
                            </thead>
                            <tbody>
                        <?php
							$query = 'user_id = ?';
							$qArr = array($user->id);

							if (isset($id)) {
								$query .= ' OR id = ?';
								$qArr[] = $id;
							}

							$count = R::count('logs', $query, $qArr);
							$qArr[] = (int)$_GET['page'] ? ((int)$_GET['page']-1)*20 : 0;
							$logs = R::findAll('logs', $query.' ORDER BY id DESC LIMIT 20 OFFSET ?', $qArr);
						?>
                        <?php if ($count > 0): ?>
							<?php $pages = ceil($count/20); ?>
                            <?php foreach ($logs as $value): ?>
                              <tr>
                                <th> <?= $value->id ?> </th>
                                <th> <?= $value->created_at ?> </th>
                                <th> <?= $value->ip ?> </th>
                                <th> <?= $value->description ?> </th>
                              </tr>
                            <?php endforeach ?>
                        <?php endif; ?>
                    <?php endif; ?>
                            <tbody>
                        </table>
                </div>
			</div>

			<ul class="pagination">
				<?php for ($i=1; $i <= $pages ?? 0; $i++) { ?>
					<li class="page-item"><a class="page-link" href="/logs.php?page=<?= $i ?>"><?= $i ?></a></li>
				<?php } ?>
			</ul>

        <footer class="mastfoot">
          <div class="inner"></div>
        </footer>
      </div>
    </div>

    <?php
    	require 'scripts.php';
    ?>
</body>
