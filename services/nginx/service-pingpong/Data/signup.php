<?php
	require 'secure.php';
	require 'db.php';
	require 'header_response.php';

	$data = $_POST;

	if ( isset($data['do_signup']) )
	{
		$errors = array();
		if ( trim($data['login']) == '' )
		{
			$errors[] = 'Введите логин';
		}

		if ( trim($data['email']) == '' )
		{
			$errors[] = 'Введите Email';
		}

		if ( $data['password'] == '' )
		{
			$errors[] = 'Введите пароль';
		}

		if ( $data['password_2'] != $data['password'] )
		{
			$errors[] = 'Повторный пароль введен не верно!';
		}

		if ( R::count('users', "login = ?", array($data['login'])) > 0)
		{
			$errors[] = 'Пользователь с таким логином уже существует!';
		}

		if ( R::count('users', "email = ?", array($data['email'])) > 0)
		{
			$errors[] = 'Пользователь с таким Email уже существует!';
		}

		if ( empty($errors) )
		{
			$user = R::dispense('users');
			$user->login = $data['login'];
			$user->email = $data['email'];

			$user->password = getHash($data['password']);
			R::store($user);
			echo '<div style="color:dreen;">Вы успешно зарегистрированы!</div><hr>';
		} else {
			http_response(400);
			echo '<div id="errors" style="color:red;">' .array_shift($errors). '</div><hr>';
		}
	}

	require 'header.php';
?>

<body>
    <div class="site-wrapper">
        <div class="cover-container">
            <?php
            	require 'navbar.php';
            ?>
		</div>

		<div class="container">
			<div class="row" style="margin-top: 150px;">
		        <div class="col-md-3 center-block">
					<form action="signup.php" method="POST" class="form-login">
						<strong>Ваш логин</strong>
						<input type="text" name="login" value="<?php echo @$data['login']; ?>" class="form-control input-sm chat-input"><br/>

						<strong>Ваш Email</strong>
						<input type="email" name="email" value="<?php echo @$data['email']; ?>" class="form-control input-sm chat-input"><br/>

						<strong>Ваш пароль</strong>
						<input type="password" name="password" value="<?php echo @$data['password']; ?>" class="form-control input-sm chat-input"><br/>

						<strong>Повторите пароль</strong>
						<input type="password" name="password_2" value="<?php echo @$data['password_2']; ?>" class="form-control input-sm chat-input"><br/>

						<button type="submit" class="btn btn-primary btn-md" name="do_signup">Регистрация</button>
					</form>
				</div>
			</div>
		</div>

		<footer class="mastfoot">
          <div class="inner"></div>
        </footer>
    </div>
	<?php
		require 'scripts.php';
	?>
</body>
