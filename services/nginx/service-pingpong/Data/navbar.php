<header class="masthead clearfix">
    <div class="inner">
        <a href="/index.php">
            <h3 class="masthead-brand">PingPong</h3>
        </a>
        <nav class="nav nav-masthead">

        <?php if ( isset ($_SESSION['logged_user']) ) : ?>
            <a class="nav-link" href="/logs.php">Логи</a>
            <span class="nav-text">Привет, <?php echo $_SESSION['logged_user']->login; ?>! </span>
            <a class="nav-link" href="logout.php">Выйти</a>
        <?php else : ?>
            <a class="nav-link" href="/login.php">Авторизация</a>
            <a class="nav-link" href="/signup.php">Регистрация</a>
        <?php endif; ?>

        </nav>
    </div>
</header>
