<?php
	require 'db.php';
	require 'header_response.php';

 	if ( isset ($_SESSION['logged_user']) ) {
		if(isset($_POST["ip"]) && !empty($_POST["ip"])){
			$ip = @preg_replace("/[\\\$|`;&<>]/", "", $_POST["ip"]);
			$response = @shell_exec("timeout 2 bash -c 'ping -c 1 ".$ip."'");
			$receive = @preg_match("/1 packets transmitted, (.*) received/s",$response,$out);

			if ($out[1]=="1") {
				http_response(200);
			} elseif ($out[1]=="0") {
				http_response(400);
			} else {
				http_response(400);
			}
		}
	}

    require 'header.php';
?>

<body>
    <div class="site-wrapper">
        <div class="cover-container">

            <?php
            	require 'navbar.php';
            ?>
			<div class="container">
				<div class="row" style="margin-top: 150px;">

            <?php if ( isset ($_SESSION['logged_user']) ) : ?>
				<div class="col-md-10 center-block">
	                <?php if(isset($_POST["ip"]) && !empty($_POST["ip"])){
	                        if ($out[1]=="1")
	                        {
								echo "<pre>$response</pre>";
								$logs = R::dispense('logs');
								$logs->user_id = $_SESSION['logged_user']->id;
								$logs->created_at = new DateTime();
								$logs->ip = $ip;
								$logs->description = $_POST["description"];
								R::store($logs);
	                        }
	                        elseif ($out[1]=="0")
	                        {
                                echo "Ping Fail";
	                        }
	                        else
	                        {
                                echo "Syntax Error";
	                        }
	                } ?>
				</div>

				<div class="col-md-3 center-block">
							<form method="POST" action="index.php"  class="form-login">
							        <input name="ip" placeholder="127.0.0.1" class="form-control input-sm chat-input" type="text">
									<br>
							        <button type="submit" class="btn btn-primary btn-md" type="submit">Отправить</button>
							</form>
						</div>
					</div>
				</div>

            <?php endif; ?>

        <footer class="mastfoot">
          <div class="inner"></div>
        </footer>
      </div>
    </div>

    <?php
    	require 'scripts.php';
    ?>
</body>
