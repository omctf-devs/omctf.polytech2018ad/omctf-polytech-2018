<?php
	require 'secure.php';
	require 'db.php';
	require 'header_response.php';

	$data = $_POST;
	if ( isset($data['do_login']) )
	{
		$user = R::findOne('users', 'login = ?', array($data['login']));
		if ( $user )
		{
			if (getHash($data['password']) == $user->password)
			{
				$_SESSION['logged_user'] = $user;
				echo '<div style="color:dreen;">Вы авторизованы!<br/> Можете перейти на <a href="/">главную</a> страницу.</div><hr>';
			} else
			{
				$errors[] = 'Неверно введен пароль!';
			}

		}else
		{
			$errors[] = 'Пользователь с таким логином не найден!';
		}

		if ( ! empty($errors) )
		{
			http_response(400);
			echo '<div id="errors" style="color:red;">' .array_shift($errors). '</div><hr>';
		}
	}
	require 'header.php';
?>
<body>
    <div class="site-wrapper">
        <div class="cover-container">
            <?php
            	require 'navbar.php';
            ?>
		</div>

		<div class="container">
			<div class="row" style="margin-top: 150px;">
		        <div class="col-md-3 center-block">
					<form action="login.php" method="POST" class="form-login">
						<input type="text" name="login" value="<?php echo @$data['login']; ?>" class="form-control input-sm chat-input" placeholder="login"><br/>

						<input type="password" name="password" value="<?php echo @$data['password']; ?>" class="form-control input-sm chat-input" placeholder="password"><br/>

						<button type="submit" class="btn btn-primary btn-md" name="do_login">Войти</button>
					</form>
				</div>
			</div>
		</div>

		<footer class="mastfoot">
          <div class="inner"></div>
        </footer>
    </div>
	<?php
		require 'scripts.php';
	?>
</body>
