from django import forms
from django.contrib.auth import get_user_model
from .models import Post,Search

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'text','flag')

class SearchForm(forms.ModelForm):

    class Meta:
        model = Search
        fields = ('search',)

class RegistrationForm(forms.Form):
    username = forms.CharField(
        min_length=6,
        max_length=128,
        widget=forms.TextInput(attrs={'placeholder': u'Введите логин'}), 
        label=u'Логин',
        help_text='Логин может содержать от 6 до 128 символов'
    )
    password = forms.CharField(
        min_length=6,
        max_length=128,
        widget=forms.PasswordInput(attrs={'placeholder': u'Введите пароль'}), 
        label=u'Пароль',
        help_text='Пороль может содержать от 6 до 128 символов'
    )
    password_repeat = forms.CharField(
        min_length=6,
        max_length=128,
        widget=forms.PasswordInput(attrs={'placeholder': u'Повторите пароль'}), 
        label=u'Повторите пароль',
        help_text='Пороль может содержать от 6 до 128 символов'
    )
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in self.fields:
            self.fields[name].widget.attrs['class'] = 'form-control'
    
    def clean_username(self):
        username = self.cleaned_data['username']
        if username and get_user_model().objects.filter(username=username).exists():
            msg = u'Такой пользователь уже существует.'
            self.add_error('username', msg)
        return username
    

    
    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        password = cleaned_data.get("password")
        password_repeat = cleaned_data.get("password_repeat")

        if password and password != password_repeat:
            msg = _("The two password fields didn't match.")
            self.add_error('password_repeat', msg)
            
    def create_user(self):
        if self.is_valid():
            username = self.cleaned_data['username']
            password = self.cleaned_data['password']
            return get_user_model().objects.create_user(
                username = username, password = password
            )
        return None
