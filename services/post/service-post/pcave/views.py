from django.shortcuts import render
from django.utils import timezone
from .models import Post,Search
from .forms import PostForm,SearchForm,RegistrationForm
from django.shortcuts import redirect
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.contrib.auth import login, logout,authenticate
from django.template import Context, Template

from django.http import HttpResponseRedirect
from django.views.generic.base import View






def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')

    return render(request, 'pcave/post_list.html', {'posts': posts,})

def post_last(request):
    posts2 = Post.objects.filter(published_date__lte=timezone.now()).filter(author=request.user).order_by('-published_date')
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')   
    return render(request, 'pcave/post_last.html', {'posts2': posts2,'posts':posts,})



def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        post = form.save(commit=False)
        post.author = request.user
        post.published_date = timezone.now()
        post.save()
        return redirect('post_list')
    else:
        form = PostForm()
    return render(request, 'pcave/post_edit.html', {'form': form})

def search(request):
    form2 = SearchForm()


    post3 = request.POST.get("search")
    posts4 = Post.objects.raw( "SELECT flag,id  FROM  pcave_post WHERE  pcave_post.title ='%s' ;" % post3   )

 
    return render(request, 'pcave/search.html', {'form2': form2, 'posts4': posts4})



class RegistrationView(FormView):
    template_name = "registration/register.html"
    form_class = RegistrationForm
    success_url = "/login/"


    def form_valid(self, form):
        form.create_user()
        user = authenticate(
            username = form.cleaned_data['username'],
            password = form.cleaned_data['password']
        )
        if user:
            login(self.request, user)
        return super().form_valid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "registration/login.html"
    success_url = "/"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)

class LogoutView(View):
    def get(self, request):
        logout(request)

        return HttpResponseRedirect("/")




