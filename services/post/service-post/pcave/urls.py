from django.conf.urls import url
from . import views
from .views import RegistrationView


urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
    url(r'^post/new/$', views.post_new, name='post_new'),
    url(r'^post/last/$', views.post_last, name='post_last'),
    url(r'^search/$', views.search, name='search'),
    url(r'^register/$', views.RegistrationView.as_view(), name='register'),
    url(r'^login/$', views.LoginFormView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
]
