from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe 
from django.contrib.auth.models import AbstractUser
from django.utils.functional import cached_property


     





class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200,blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)
    flag = models.CharField(max_length=200,blank=True, null=True)


    def publish(self):
        self.published_date = timezone.now()
        self.save()


    def display_my_safefield(self): 
        return mark_safe(self.text)

    def __str__(self):
        return self.flag




class Search(models.Model):
    search = models.CharField(max_length=200,blank=True, null=True)




