#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import requests
import requests.utils
import pickle
import random
import string
from fake_useragent import UserAgent
from sys import argv,exc_info
from hashlib import md5
import re

ERROR   = 110
SUCCESS = 101
CORRUPT = 102
MUMBLE  = 103
DOWN    = 104

DEBUG = True
PORT = "2222"
ua = UserAgent()

def randstr():
    n = int(random.uniform(10, 50))
    a = string.ascii_letters + string.digits
    return(str(''.join([random.choice(a) for i in range(n)])))

def make_md5(s, encoding='utf-8'):
    return md5(s.encode(encoding)).hexdigest()

def string2numeric_hash(text, encoding='utf-8'):
    return int(md5(text.encode(encoding)).hexdigest()[:8], 16)

def check(hostname):
    try:
        r = requests.get('http://%s:%s/index.php' % (hostname, PORT), headers={'User-Agent': ua.random},timeout=5)

        if (r.status_code != 200):
            return(DOWN)

        return(SUCCESS)
    except requests.exceptions.ConnectionError:
        return(DOWN)
    except:
        print('unknown err', exc_info())
        return(ERROR)

def put(hostname, uid, flag):
    try:
        HEADER = {'User-Agent': ua.random}
        url = 'http://%s:%s' % (hostname, PORT)
        put = False
        with requests.Session() as s:
            regData = {
                'email':      randstr(),
                'login':      make_md5(uid+'pCTF18log'),
                'password':   string2numeric_hash(uid+'pCTF18pass'),
                'password_2': string2numeric_hash(uid+'pCTF18pass'),
                'do_signup':  ''
            }
            r = s.post(url + '/signup.php',data=regData, headers=HEADER, timeout=5)

            if (r.status_code != 200):
                return(MUMBLE)

            logData = {
                'login'    : make_md5(uid+'pCTF18log'),
                'password' : string2numeric_hash(uid+'pCTF18pass'),
                'do_login' : ''
            }
            l = s.post(url + '/login.php',data=logData, headers=HEADER, timeout=5)

            if (l.status_code != 200):
                return(MUMBLE)

            pingData = {
                'ip':          '127.0.0.1',
                'description': flag
            }
            p = s.post(url + '/index.php',data=pingData, headers=HEADER, timeout=5)

            if (p.status_code != 200):
                return(MUMBLE)
            else:
                put = True

        if put:
            return(SUCCESS)

        return(MUMBLE)
    except requests.exceptions.ConnectionError:
        return(DOWN)
    except:
        print('unknown err', exc_info())
        return(ERROR)

def get(hostname, uid, flag):
    try:
        HEADER = {'User-Agent': ua.random}
        url = 'http://%s:%s' % (hostname, PORT)
        get = False
        with requests.Session() as s:
            logData = {
                'login'    : make_md5(uid+'pCTF18log'),
                'password' : string2numeric_hash(uid+'pCTF18pass'),
                'do_login' : ''
            }
            l = s.post(url + '/login.php',data=logData, headers=HEADER, timeout=5)

            if (l.status_code != 200):
                return(MUMBLE)

            g = s.get(url + '/logs.php', headers=HEADER, timeout=5)

            if (g.status_code != 200):
                return(MUMBLE)

            if re.search(r''+flag+'', str(g.text)):
                get = True
            else:
                return(CORRUPT)

        if get:
            return(SUCCESS)
    except requests.exceptions.ConnectionError:
        return(DOWN)
    except:
        print('unknown err', exc_info())
        return(ERROR)

if __name__ == '__main__':
    res = False
    if len(argv) > 1:
        if argv[1] == "check":
            if len(argv) > 2:
                res = check(argv[2])
        elif argv[1] == "put":
            if len(argv) > 4:
                res = put(argv[2], argv[3], argv[4])
        elif argv[1] == "get":
            if len(argv) > 4:
                res = get(argv[2], argv[3], argv[4])
        if res:
            if DEBUG:
                print(res)
            exit(res)
    exit(ERROR)
