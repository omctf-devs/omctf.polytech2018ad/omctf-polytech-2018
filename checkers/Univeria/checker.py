#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import requests
import requests.utils
import pickle
import random
import string
from fake_useragent import UserAgent
from sys import argv,exc_info
from hashlib import md5
from socketIO_client import SocketIO, LoggingNamespace
from socketIO_client.exceptions import ConnectionError
import re
import json
import ssl

ERROR   = 110
SUCCESS = 101
CORRUPT = 102
MUMBLE  = 103
DOWN    = 104

DEBUG = True
PORT = 3000
ua = UserAgent()

classes = ['Rogue','Mage','War']
potion = []

def randstr():
    n = int(random.uniform(10, 50))
    a = string.ascii_letters + string.digits
    return(str(''.join([random.choice(a) for i in range(n)])))

def make_md5(s, encoding='utf-8'):
    return md5(s.encode(encoding)).hexdigest()

def check(hostname):
    try:
        r = requests.get('http://%s:%s' % (hostname, PORT), headers={'User-Agent': ua.random},timeout=5)

        if (r.status_code != 200):
            return(DOWN)

        return(SUCCESS)
    except requests.exceptions.ConnectionError:
        return(DOWN)
    except:
        print('unknown err', exc_info())
        return(ERROR)

def onSignIn(*args):
    try:
        if not args[0]['success']:
            raise Exception(MUMBLE)
    except:
        raise Exception(MUMBLE)

def onSignUp(*args):
    try:
        if not args[0]['success']:
            raise Exception(MUMBLE)
    except:
        raise Exception(MUMBLE)

def onShowQuest(*args):
    try:
        if not args[0]['title']:
            raise Exception(MUMBLE)
    except:
        raise Exception(MUMBLE)

def onShowShop(*args):
    try:
        if isinstance(args, list):
            raise Exception(MUMBLE)
    except:
        print(exc_info())
        raise Exception(MUMBLE)

def onShowInventory(*args):
    try:
        if not args[0][0]['id']:
            raise Exception(MUMBLE)
        else:
            for it in args[0]:
                if it['name'] == 'potion':
                    global potion
                    potion = it
    except:
        raise Exception(MUMBLE)

def put(hostname, uid, flag):
    try:
        put = False
        clss = classes[random.randint(0,2)]
        login = make_md5(uid+'pCTF18log')
        password = make_md5(uid+'pCTF18pass')
        if clss == 'Mage':
            str = 10
            int = 15
            agi = 10
        if clss == 'Rogue':
            str = 10
            int = 10
            agi = 15
        if clss == 'War':
            str = 15
            int = 10
            agi = 10

        with SocketIO(hostname, PORT, wait_for_connection=False) as socketIO:
            socketIO.on('signUpResponse', onSignUp)
            socketIO.emit('signUp', {
                "username":login,
                "password":password,
                "class": clss,
                "str": str,
                "int": int,
                "agi": agi
                })
            socketIO.wait(seconds=3)

            socketIO.on('signInResponse', onSignIn)
            socketIO.emit('signIn', {
                "username":login,
                "password":password
            })
            socketIO.wait(seconds=3)

            socketIO.on('showQuest', onShowQuest)
            socketIO.emit('showQuest', {})
            socketIO.wait(seconds=3)

            socketIO.emit('getQuest', {})

            socketIO.on('showShop', onShowShop)
            socketIO.emit('showShop', {})
            socketIO.wait(seconds=3)

            socketIO.on('showInventory', onShowInventory)
            socketIO.emit('showInventory', {})
            socketIO.wait(seconds=3)

            global potion
            if potion['id']:
                socketIO.emit('Sell', potion['id'], flag, 1000000)
                socketIO.wait(seconds=3)
                put = True

        if put:
            return(SUCCESS)

        return(MUMBLE)
    except ConnectionError:
        print('err', exc_info())
        return(DOWN)
    except Exception as err:
        if err.args[0] == DOWN:
            return(DOWN)
        if err.args[0] == MUMBLE:
            print(exc_info())
            return(MUMBLE)
        print('unknown err', exc_info())
        return(ERROR)
    except:
        print('unknown err', exc_info())
        return(ERROR)

def get(hostname, uid, flag):
    try:
        login = make_md5(uid+'pCTF18log')
        password = make_md5(uid+'pCTF18pass')
        get = False
        with SocketIO(hostname, PORT, wait_for_connection=False) as socketIO:
            socketIO.on('signInResponse', onSignIn)
            socketIO.emit('signIn', {
                "username":login,
                "password":password
            })
            socketIO.wait(seconds=3)

            socketIO.on('showInventory', onShowInventory)
            socketIO.emit('showInventory', {})
            socketIO.wait(seconds=3)
            global potion

            try:
                if not potion['description']:
                    return(MUMBLE)
            except:
                return(MUMBLE)

            if potion['description'] != flag:
                return(CORRUPT)
            get = True
        if get:
            return(SUCCESS)
        return(MUMBLE)
    except ConnectionError:
        return(DOWN)
    except Exception as err:
        if err.args[0] == DOWN:
            return(DOWN)
        if err.args[0] == MUMBLE:
            return(MUMBLE)
        print('unknown err', exc_info())
        return(ERROR)
    except:
        print('unknown err', exc_info())
        return(ERROR)

if __name__ == '__main__':
    res = False
    if len(argv) > 1:
        if argv[1] == "check":
            if len(argv) > 2:
                res = check(argv[2])
        elif argv[1] == "put":
            if len(argv) > 4:
                res = put(argv[2], argv[3], argv[4])
        elif argv[1] == "get":
            if len(argv) > 4:
                res = get(argv[2], argv[3], argv[4])
        if res:
            if DEBUG:
                print(res)
            exit(res)
    exit(ERROR)
